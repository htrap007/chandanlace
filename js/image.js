var images = [
    {
        id: GetRandomId(), name: 'image-1', src: "../img/lace1.jpg", description: 'this is image 1',
        subImages: [
            { id: 1, src: '../img/lace11.jpg' },
            { id: 2, src: '../img/lace12.jpg' },
            { id: 3, src: '../img/lace13.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-2', src: '../img/lace2.jpg', description: 'this is image 2',
        subImages: [
            { id: 1, src: '../img/lace14.jpg' },
            { id: 2, src: '../img/lace15.jpg' },
            { id: 3, src: '../img/lace16.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-3', src: '../img/lace3.jpg', description: 'this is image 2',
        subImages: [
            { id: 1, src: '../img/lace17.jpg' },
            { id: 2, src: '../img/lace18.jpg' },
            { id: 3, src: '../img/lace19.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-4', src: '../img/lace4.jpg', description: 'this is image 2',
        subImages: [
            { id: 1, src: '../img/lace20.jpg' },
            { id: 2, src: '../img/lace21.jpg' },
            { id: 3, src: '../img/lace22.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-5', src: '../img/lace5.jpg', description: 'this is image 2',
        subImages: [
            { id: 1, src: '../img/lace1.jpg' },
            { id: 2, src: '../img/lace2.jpg' },
            { id: 3, src: '../img/lace3.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-6', src: '../img/lace6.jpg', description: 'this is image 2',
        subImages: [
            { id: 1, src: '../img/lace4.jpg' },
            { id: 2, src: '../img/lace5.jpg' },
            { id: 3, src: '../img/lace6.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-7', src: '../img/lace7.jpg', description: 'this is image 2',
        subImages: [
            { id: 1, src: '../img/lace7.jpg' },
            { id: 2, src: '../img/lace8.jpg' },
            { id: 3, src: '../img/lace9.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-8', src: '../img/lace8.jpg', description: 'this is image 2',
        subImages: [
            { id: 1, src: '../img/lace10.jpg' },
            { id: 2, src: '../img/lace12.jpg' },
            { id: 3, src: '../img/lace13.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-9', src: '../img/lace9.jpg', description: 'this is image 2',
        subImages: [
            { id: 1, src: '../img/lace18.jpg' },
            { id: 2, src: '../img/lace19.jpg' },
            { id: 3, src: '../img/lace20.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-10', src: '../img/lace10.jpg', description: 'this is image 3',
        subImages: [
            { id: 1, src: '../img/lace1.jpg' },
            { id: 2, src: '../img/lace2.jpg' },
            { id: 3, src: '../img/lace3.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-11', src: "../img/lace1.jpg", description: 'this is image 1',
        subImages: [
            { id: 1, src: '../img/lace11.jpg' },
            { id: 2, src: '../img/lace12.jpg' },
            { id: 3, src: '../img/lace13.jpg' }
        ]
    },
    {
        id: GetRandomId(), name: 'image-12', src: '../img/lace2.jpg', description: 'this is image 2',
        subImages: [
            { id: 1, src: '../img/lace14.jpg' },
            { id: 2, src: '../img/lace15.jpg' },
            { id: 3, src: '../img/lace16.jpg' }
        ]
    }
];
function GetRandomId() {
    return parseInt((Math.random() * 999) + 1);
}
$(document).ready(function () {
    var html = '';
    for (i = 0; i < images.length; i++) {
        var image = images[i];
        html += '<div class="col-lg-3 col-md-4 col-sm-6 portfolio-item pt-3">';
        html += '<div class="card h-100 ">';
        html += '<img class="card-img-top image" src="' + image.src + '" data-toggle="modal" data-target="#' + image.id + '" alt=""  height="150px">';
        html += ' <div class="middle">';
        html += '<button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#' + image.id + '"><i class="fa fa-eye" aria-hidden="true"></i> Quick View</button>';
        html += '</div>';
        html += '<div class="card-body">';
        html += '<h5 class="card-title">';
        html += image.name;
        html += '</h5>';
        html += '<p class="card-text" style="font-size:14px">';
        html += image.description;
        html += '</p>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '<div class="modal fade" id="' + image.id + '" role="dialog">';
        html += '<div class="modal-dialog">'
        html += '<div class="modal-content">';
        html += '<div class="modal-header">';
        html += '<h4 class="modal-title">' + image.name + '</h4>';
        html += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
        html += '</div>';
        html += '<div class="modal-body">';
        html += '<div class="container">';
        html += '<div id="subImage' + image.id + '" class="carousel slide" data-ride="carousel">';
        html += '<div class="carousel-inner">';


        for (j = 0; j < image.subImages.length; j++) {
            var subimage = images[i].subImages[j];
            if (j == 0) {
                html += '<div class="carousel-item active">';
            }
            else {
                html += '<div class="carousel-item">';
            }
            html += '<img class="d-block w-100" height="200" src="' + subimage.src + '" alt="First slide">';
            html += '</div>';
        }
        if (image.subImages && image.subImages.length > 1) {
            html += '<a class="carousel-control-prev" href="#subImage' + image.id + '" role="button" data-slide="prev">';
            html += '<span class="carousel-control-prev-icon" aria-hidden="true"></span>';
            html += '<span class="sr-only">Previous</span>';
            html += '</a>';
            html += '<a class="carousel-control-next" href="#subImage' + image.id + '" role="button" data-slide="next">';
            html += '<span class="carousel-control-next-icon" aria-hidden="true"></span>';
            html += '<span class="sr-only">Next</span>';
            html += '</a>';
        }
        html += '</div>';
        html += '</div>';
        html += '</div >';
        html += '</div >';
        html += '<div class="modal-footer">';
        html += '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        html += '</div>';
        html += '</div >';
        html += '</div >';
        html += '</div >';
    }



    $('#portfolio').html(html);
})
